(function ($) {
  if (!Object.create) {
    Object.create = function (o) {
      function F () {
      }

      F.prototype = o;
      return new F();
    };
  }

  if (!Function.setInheritance) {
    /**
     * @param {Function} Class The Class you are creating and extending.
     *
     * @param {Function} SuperClass The SuperClass that the new Class will
     *   inherit from.
     *
     * @param {Array} args An array of arguments that will be passed to the
     *   constructor necessary for successful instantiation.
     */
    Function.setInheritance = function (Class, SuperClass, args) {
      Class.prototype = Object.create(SuperClass);
      Class.prototype.constructor = Class;
      SuperClass.apply(Class, args);
    };
  }

  /**
   * A simple jQuery plugin to iterate over an HTML element's classes.
   *
   * @copyright Copyright (C) 2013 Anand Pathak Sharma
   *
   * @license http://opensource.org/licenses/MIT
   *
   * @requires jQuery >= 1.3.2
   *
   * @description Types of arguments allowed:
   *   1) Pass in two strings:
   *      {methodName} The name of the internal method that should be called.
   *      {searchString} The string that should be searched for in each class.
   *      E.g. $('li').findWithClass('startingWith', 'views-');
   *
   *   2) Pass in a hash (associative array) of key-value pairs that requires
   *      two keys: { filterMethods : [ methodName : 'searchString' ],
   *      concatResults : true }.
   *
   *      The 'concatResults' defaults to true which means that each
   *      filterMethod will be run on the *original collection* and and the
   *      resulting collection will include all elements found.
   *
   *      If 'concatResults' is set to false then each filterMethod will
   *      further *refine* each resulting collection, further narrowing
   *      results returned each time. It is important to note that the order
   *      that the filterMethods are specified becomes important in this case.
   *
   * @todo Further work to be done allowing for the passing of an option hash
   *   that will allow for begins-with and ends-with strings.
   *
   * @param prefix (String) The string that the class you are
   *   looking for begins with.
   *
   * @return Array An array of matched classes.
   */
  $.fn.findWithClass = function () {

    /**
     * Private Variables
     *
     * @private {jQuery} _subset A jQuery object collection of
     *   HTML elements. A place to store historical results for
     *   further pruning.
     *
     * @private {RegExp} _regex The RegExp object used for a given
     *   "findWithClass" call.
     *
     * @private {jQuery} _results The jQuery object collection of the
     *   last search operation.
     *
     *
     *************************************************************************/
    var
      _matches,
      _regex,
      _result,
      _elementCollection,
      _callableMethods = [
        'startingWith',
        'endingWith',
        'containing'
      ];

    /**
     * Public Variables
     *************************************************************************/
    this.options = {
      concatResults : true,
      filterMethods : {}
    };

    /**
     * Private Methods
     *************************************************************************/
    /**
     * Filters the jQuery Collection set in the _elementCollection
     * variable by applying Regular Expression in the _regex variable
     * against it.
     */
    var _execute = function (regex) {
      _matches = $();
      _elementCollection.each(function () {
        var
        // Instantiate a jQuery object using the current element.
          currentElement = $(this),
          // Setting this to false in the case where the currentElement
          // hasn't got a `class` attribute set at all.
          classAttr = currentElement.attr('class') || false,
          // If the element has classes, split them up for iteration.
          classes = (classAttr) ? classAttr.split(' ') : false;

        if(classes) {
          for (var i = 0; i < classes.length; i++) {
            if (regex.test(classes[i])) {
              _matches = _matches.add(currentElement[0]);
              break;
            }
          }
        }
      });
    };

    /**
     *  Public Methods
     *************************************************************************/
    /**
     * Retrieves the value of the private variable <code>_matches</code>.
     */
    this.getResult = function () {
      return _result;
    };

    /**
     * Sets the value of the private variable <code>_matches</code>.
     *
     * @param {jQuery} collection A jQuery element collection that
     *   will be returned by this plugin at the end of execution.
     */
    this.setResult = function (collection) {
      _result = collection;
    };

    /**
     * @type {Array}
     * @return Returns a list of "callable" methods within this plugin.
     */
    this.getCallable = function () {
      return _callableMethods;
    };

    /**
     * @return {RegExp} Returns RegEx object.
     * @see setRegex
     */
    this.getRegex = function () {
      return _regex;
    };

    /**
     * @param {string} expression A RegexObject-compatible string.
     */
    this.setRegex = function (expression) {
      return _regex = new RegExp(expression);
    };

    /**
     * @param {string} expression The string that the HTML Element's
     *   class should <strong>begin</strong> with.
     */
    this.startingWith = function (expression) {
      this.setRegex('^' + expression);
      _execute(this.getRegex());
    };

    /**
     * @param {string} expression The string that the HTML Element's
     *   class should <strong>end</strong> with.
     */
    this.endingWith = function (expression) {
      this.setRegex(expression + '$');
      _execute(this.getRegex());
    };

    /**
     * @param {string} expression The string that the HTML Element's
     *   class should begin with.
     */
    this.containing = function (expression) {
      this.setRegex(expression);
      _execute(this.getRegex());
    };

    /**
     * Runs a single method against the search term specified then updates the
     * private variable <code>_matches</code> which is in turn used to set
     * the final value of <code>_result</code> - the jQuery collection that is
     * returned by this plugin.
     *
     * @param {String} methodName The name of the filter method that should be
     *   run.
     *
     * @param {String} searchString The desired search terms. The string should
     *   be in a JavaScript RegExp object compatible format.
     */
    this.executeMethod = function (methodName, searchString) {
      this[methodName](searchString);
      this.setResult(_matches);
    };

    /**
     * Run each filter method that has been specified and concatenate results
     * as specified by the <code>concatResults</code> boolean variable.
     *
     * @param {Object} options Hash of options specifying filter methods
     *   opposite the search string that should be used with each method and
     *   a boolean <code>concatResults</code> to specify whether results are
     *   additive or subtractive.
     *
     * @see $.fn.findWithClass
     */
    this.executeManyMethods = function (options) {

      for (var methodName in options.filterMethods) {
        var searchString = options.filterMethods[methodName];

        if ($.inArray(methodName, this.getCallable()) > -1 && typeof this[methodName] == 'function') {
          if (options.concatResults) {
            // Case: Results should be "additive".
            var
              newResults,
              existingMatches = this.getResult() || $();

            this.executeMethod(methodName, searchString);

            newResults = existingMatches.add(_matches);

            this.setResult(newResults);
          }
          else {
            // Case: Results should be "subtractive".
            if (typeof _result !== 'undefined' && _result.length > 0) {
              _elementCollection = _result;
            }

            this.executeMethod(methodName, searchString);

            this.setResult(_matches);
          }
        }
      }
    };

    /**
     * Initialise the findWithClass object and call appropriate method
     * based upon case detected (implementing Polymorphism).
     *
     * @param {Array} args The arguments that are passed into the Class
     *   constructor. There are several possible combinations explained in the
     *   Description.
     */
    this.init = function (args) {
      var
        firstArg = args[0],
        secondArg = args[1];

      if (args.length == 0) {
        /**
         * Return original collection if no arguments were provided. Allows
         * for the setup of sub-plugins on $.fn for more specific use.
         */
        return _result = this;
      } else if (args.length == 1 && typeof firstArg == 'string') {
        /**
         * User gave us only one of two strings needed.
         *
         * If strings are being provided as the arguments, the first needs to
         * be a valid method name and the second to be the RegEx string
         * that will be passed to the called method.
         *
         * The User may use $.findWithClass.getCallable() to get a list of
         * method names this Plugin supports.
         */
        throw new SyntaxError('Error in $.findWithClass -> Received only one of two String arguments. Please check the documentation for proper use.');
      }
      else if (
        typeof firstArg == 'string' &&
          typeof secondArg == 'string' &&
          typeof this[firstArg] == 'function'
        ) {
        if ($.inArray(firstArg, this.getCallable()) > -1 && typeof this[firstArg] == 'function') {
          this.executeMethod(firstArg, secondArg);
        }
      }
      else if (typeof firstArg == 'object') {
        if (
          !$.inArray('filterMethods', firstArg) ||
            firstArg.filterMethods.length == 0
          ) {
          throw new SyntaxError('Error in $.findWithClass -> Object passed in did not specify any filtering values.')
        }
        /**
         * Sanity checks complete, we have what seems to be a reasonable hash
         * of options to run this plugin with. Merge user-values with defaults
         * then run the requested methods.
         */
        this.options = $.extend(this.options, firstArg);
        this.executeManyMethods(this.options);
      }

      return this;
    };

    // Ensure this function is run against a jQuery Element Collection Object. E.g.: $('#someUl > li');
    if (this instanceof jQuery) {
      // Set defaults for private variables.
      _elementCollection = this;

      this.init(arguments);
    } else {
      throw new TypeError('Error in $.findWithClass -> $.findWithClass and its sub-plugins need to be run against a jQuery element collection.');
    }

    return _result;
  };

  $.fn.findWithClassStartingWith = function (expression) {
    return $(this).findWithClass('startingWith', expression);
  };

  $.fn.findWithClassEndingWith = function (expression) {
    return $(this).findWithClass('endingWith', expression);
  };

  $.fn.findWithClassContaining = function (expression) {
    return $(this).findWithClass('containing', expression);
  };
})(jQuery);
